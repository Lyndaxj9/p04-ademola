Omowumi Lynda Ademola 

CS441 Mobile Game Design 

March 18, 2017

Assignment 04

Create a side runner game using SpriteKit

commit f30f05aa69bb48bc5645452d6e5fb72faac2d566
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Sat Mar 18 22:19:33 2017 -0400

    added readme

commit 648023b24ea4c7e4cac6502cd791832a8a94a33a
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Sat Mar 18 01:43:56 2017 -0400

    changes to platform generation

commit f30329daab1694d4488d6d3f8c56883db6b57fb9
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Fri Mar 17 19:55:33 2017 -0400

    moving platforms

commit ec1dc4ebe35a1962d568979f950c0b804beaac16
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Fri Mar 17 19:35:04 2017 -0400

    generate random platforms

commit 2fe9d5c86628f139a544cffa3e5234f975da3c69
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Tue Mar 14 20:38:42 2017 -0400

    character jumps

commit a4ef3ba63ed2a4f7183d2c14612a3d99e7a9c48a
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Tue Mar 14 20:06:25 2017 -0400

    started adding platforms

commit 7c8ded5a0e7992eaf3cd3bdefe8b2297ef376250
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Tue Mar 14 19:34:03 2017 -0400

    moved background logic to SKNode

commit 83a85341c220ebbdb87ad55c7732fd8d84e1b23e
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Mon Mar 13 23:52:58 2017 -0400

    opening screen and skspritenodes

commit df8b91599c64d96120deebfeeebe1830b5265ca1
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Mon Mar 13 15:27:38 2017 -0400

    figuring out platforms

commit 3e407ab5196194282d080920dbbcf3e216407e24
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Sun Mar 12 01:15:33 2017 -0500

    figured out physicsbodies

commit 237644a03bf2f89684feee5889ce06f34b741486
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Sun Mar 12 00:50:52 2017 -0500

    figuring out physicsbodys

commit 26b16fdf853d32e152655931dec131dbdb50b2c7
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Fri Mar 10 23:09:10 2017 -0500

    working on animating sprite

commit 0a922cc470067899bc1e0d05fc52d0a9cdcb51b9
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Thu Mar 9 19:43:30 2017 -0500

    set up background

commit 37233faa17b15afffd461338533c0204b4fff12e
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Thu Mar 9 15:57:17 2017 -0500

    started adding background

commit ff5ff16cb9fe1ce0e61e8c8e61b41c0185ae6ff6
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Thu Mar 9 01:07:13 2017 -0500

    init commit