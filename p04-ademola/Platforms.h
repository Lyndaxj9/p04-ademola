//
//  Platforms.h
//  p04-ademola
//
//  Created by Lynda on 3/14/17.
//  Copyright © 2017 Lynda. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface Platforms : SKNode
@property (nonatomic) SKTextureAtlas *tps;
@property (nonatomic) SKTexture *tp1, *tp2, *tp3, *tp4, *tp5, *tp6, *tp7, *tp8, *tp9,
                                *tp10, *tp11, *tp12, *tp13, *tp14, *tp15, *tp16;
//pf:Platform Fat, pb:Platform Big, ps:Platform Skinny
@property (nonatomic) SKSpriteNode *pf0, *pf1, *pf2, *pb0, *ps0, *ps1;
@property (nonatomic, strong) NSMutableArray *allPlatforms;

- (void)update;
@end
