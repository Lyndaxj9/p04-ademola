//
//  Platforms.m
//  p04-ademola
//
//  Created by Lynda on 3/14/17.
//  Copyright © 2017 Lynda. All rights reserved.
//

#import "Platforms.h"

@implementation Platforms

- (id)init
{
    self = [super init];
    
    if(self)
    {
        //initalize tiles
        _tps = [SKTextureAtlas atlasNamed:@"tilesplatform"];
        _tp1 = [_tps textureNamed:@"1"];
        _tp2 = [_tps textureNamed:@"2"];
        _tp3 = [_tps textureNamed:@"3"];
        _tp4 = [_tps textureNamed:@"4"];
        _tp5 = [_tps textureNamed:@"5"];
        _tp6 = [_tps textureNamed:@"6"];
        _tp7 = [_tps textureNamed:@"7"];
        _tp8 = [_tps textureNamed:@"8"];
        _tp9 = [_tps textureNamed:@"9"];
        _tp10 = [_tps textureNamed:@"10"];
        _tp11 = [_tps textureNamed:@"11"];
        _tp12 = [_tps textureNamed:@"12"];
        _tp13 = [_tps textureNamed:@"13"];
        _tp14 = [_tps textureNamed:@"14"];
        _tp15 = [_tps textureNamed:@"15"];
        _tp16 = [_tps textureNamed:@"16"];
        
        _allPlatforms = [NSMutableArray array];
        
        [self generatePlatforms];
        NSLog(@"number objects in array: %lu", (unsigned long)[_allPlatforms count]);
        /*
        for(SKSpriteNode* plat in _allPlatforms){
            [self addChild:plat];
            NSLog(@"add platform");
        }*/
        /*
        [self createFatPlatformshort];
        [self addChild:_pf0];
        [self createFatPlatformmedium];
        [self addChild:_pf1];
        [self createFatPlatformlong];
        [self addChild:_pf2];
         */
    }
    
    return self;
}

- (SKSpriteNode *)createFatPlatformshort:(float)xValue withYValue:(float)yValue
{
    //platform fat short
    _pf0 = [SKSpriteNode spriteNodeWithTexture:_tp1];
    _pf0.physicsBody = [SKPhysicsBody bodyWithTexture:_tp1 size:_tp1.size];
    _pf0.physicsBody.dynamic = NO;
    _pf0.position = CGPointMake(xValue, yValue);
    _pf0.zPosition = 10;
    _pf0.xScale = 0.25;
    _pf0.yScale = 0.25;
    _pf0.name = @"platform";
    
    SKSpriteNode *plt03 = [SKSpriteNode spriteNodeWithTexture:_tp3];
    plt03.physicsBody = [SKPhysicsBody bodyWithTexture:_tp3 size:_tp3.size];
    plt03.physicsBody.dynamic = NO;
    plt03.position = CGPointMake(128, 0);
    plt03.zPosition = 10;
    [_pf0 addChild:plt03];
    
    return _pf0;
}

- (SKSpriteNode *)createFatPlatformmedium:(float)xValue withYValue:(float)yValue
{
    //platform fat
    _pf1 = [SKSpriteNode spriteNodeWithTexture:_tp1];
    _pf1.physicsBody = [SKPhysicsBody bodyWithTexture:_tp1 size:_tp1.size];
    _pf1.physicsBody.dynamic = NO;
    _pf1.position = CGPointMake(xValue, yValue);
    _pf1.zPosition = 10;
    _pf1.xScale = 0.25;
    _pf1.yScale = 0.25;
    _pf1.name = @"platform";
    
    
    SKSpriteNode *plt02 = [SKSpriteNode spriteNodeWithTexture:_tp2];
    plt02.physicsBody = [SKPhysicsBody bodyWithTexture:_tp2 size:_tp2.size];
    plt02.physicsBody.dynamic = NO;
    plt02.position = CGPointMake(128, 0);
    plt02.zPosition = 10;
    [_pf1 addChild:plt02];
    
    SKSpriteNode *plt03 = [SKSpriteNode spriteNodeWithTexture:_tp3];
    plt03.physicsBody = [SKPhysicsBody bodyWithTexture:_tp3 size:_tp3.size];
    plt03.physicsBody.dynamic = NO;
    plt03.position = CGPointMake(128, 0);
    plt03.zPosition = 10;
    [plt02 addChild:plt03];
    
    return _pf1;

}

- (SKSpriteNode *)createFatPlatformlong:(float)xValue withYValue:(float)yValue
{
    //platform fat long
    /*
    _pf2 = [SKSpriteNode spriteNodeWithTexture:_tp1];
    _pf2.physicsBody = [SKPhysicsBody bodyWithTexture:_tp1 size:_tp1.size];
    _pf2.physicsBody.dynamic = NO;
    _pf2.position = CGPointMake(xValue, yValue);
    _pf2.zPosition = 10;
    _pf2.xScale = 0.25;
    _pf2.yScale = 0.25;
    _pf2.name = @"platform";
    */
    
    SKSpriteNode *p = [SKSpriteNode spriteNodeWithTexture:_tp1];
    p.physicsBody = [SKPhysicsBody bodyWithTexture:_tp1 size:_tp1.size];
    p.physicsBody.dynamic = NO;
    p.position = CGPointMake(xValue, yValue);
    p.zPosition = 10;
    p.xScale = 0.25;
    p.yScale = 0.25;
    p.name = @"platform";
    
    SKSpriteNode *plt02 = [SKSpriteNode spriteNodeWithTexture:_tp2];
    plt02.physicsBody = [SKPhysicsBody bodyWithTexture:_tp2 size:_tp2.size];
    plt02.physicsBody.dynamic = NO;
    plt02.position = CGPointMake(128, 0);
    plt02.zPosition = 10;
    [p addChild:plt02];
    
    SKSpriteNode *plt12 = [plt02 copy];
    [plt02 addChild:plt12];
    
    SKSpriteNode *plt03 = [SKSpriteNode spriteNodeWithTexture:_tp3];
    plt03.physicsBody = [SKPhysicsBody bodyWithTexture:_tp3 size:_tp3.size];
    plt03.physicsBody.dynamic = NO;
    plt03.position = CGPointMake(128, 0);
    plt03.zPosition = 10;
    [plt12 addChild:plt03];
    
    return p;
}

- (float)skRandf
{
    return (float)arc4random_uniform(RAND_MAX)/RAND_MAX;
}

- (CGFloat)skRandInRange:(CGFloat)low withMaxFloat:(CGFloat)high
{
    return ([self skRandf] * (high - low) + low);
}

- (int)aRandom
{
    return (arc4random() % 2 ? 1 : -1);
}

- (void)generatePlatforms
{
    
    CGFloat radiusX, radiusY;
    //CGFloat angle;
    CGFloat x, y;
    //CGFloat x = radius * cosf(angle);
    //CGFloat y = radius * sinf(angle);
    //x = fabs(x);
    //y = fabs(y);
    CGFloat prevX = -50;
    CGFloat prevY = 100;
    
    int i;
    for(i = 0; i < 10; i++){
        if([_allPlatforms count] > 0){
            CGPoint prevPos = [[_allPlatforms lastObject] position];
            prevX = prevPos.x;
            prevY = prevPos.y;
        }
        
        radiusX = [self skRandInRange:100 withMaxFloat:150];
        radiusY = [self skRandInRange:150 withMaxFloat:300];
        x = radiusX;
        y = radiusY * [self aRandom];
        
        while(prevY+y < 115 || prevY+y > 375) {
            radiusY = [self skRandInRange:25 withMaxFloat:100];
            y = radiusY * [self aRandom];
        }
        NSLog(@"NGen'd x: %f | y: %f", x, y);
        //[_allPlatforms addObject:[self createFatPlatformlong:prevX+x withYValue:prevY+y]];
        NSLog(@"New platform position x: %f | y: %f", prevX+x, prevY+y);
        int randPlat = [self skRandInRange:0 withMaxFloat:3];
        
        SKSpriteNode *newPlat;
        
        if(randPlat == 0){
            newPlat = [self createFatPlatformmedium:prevX+x withYValue:prevY+y];
        } else if(randPlat == 1){
            newPlat = [self createFatPlatformlong:prevX+x withYValue:prevY+y];
        } else if(randPlat == 2){
            newPlat = [self createFatPlatformshort:prevX+x withYValue:prevY+y];
        } else {
           newPlat = [self createFatPlatformmedium:prevX+x withYValue:prevY+y];
        }
        [_allPlatforms addObject:newPlat];
        [self addChild:newPlat];
    }
}

- (void)update
{
    for(SKSpriteNode* plat in _allPlatforms){
        plat.position = CGPointMake(plat.position.x-25, plat.position.y);
        if(plat.position.x < -100){
            [plat removeFromParent];
        }
    }
    SKSpriteNode *lastPlat = [_allPlatforms lastObject];
    CGPoint lastPos = [lastPlat position];
    if(lastPos.x < 800){
        [self generatePlatforms];
    }

}

@end
