//
//  StartScene.h
//  p04-ademola
//
//  Created by Lynda on 3/4/17.
//  Copyright © 2017 Lynda. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "BKG.h"
#import "Platforms.h"

@interface StartScene : SKScene{
    BKG *aBKG;
    Platforms *aPlatforms;
    SKSpriteNode *ava;
    SKAction *mj;
}
@end
    
