//
//  BKG.m
//  p04-ademola
//
//  Created by Lynda on 3/14/17.
//  Copyright © 2017 Lynda. All rights reserved.
//

#import "BKG.h"

@implementation BKG

static const uint32_t groundCategory = 0x1 << 0;
static const uint32_t playerCategory = 0x1 << 1;

- (BKG *) init
{
    self = [super init];
    
    if(self)
    {
        //BKG *aNode = [[BKG alloc]init];
    }
    
    return self;
}

- (BKG *) initWithSize:(CGSize)textureSize
{
    self = [super init];
    
    if(self)
    {
        SKTextureAtlas *bgs = [SKTextureAtlas atlasNamed:@"background"];
        SKTexture *bg0 = [bgs textureNamed:@"layer_05"];
        SKTexture *mt0 = [bgs textureNamed:@"layer_03"];
        SKTexture *gd0 = [bgs textureNamed:@"layer_01"];
        
        //main background01
        _mainbg = [SKSpriteNode spriteNodeWithTexture:bg0 size:textureSize];
        _mainbg.anchorPoint = CGPointZero;
        _mainbg.position = CGPointMake(0, 0);
        _mainbg.name = @"layer05";
        
        //main background02
        _mainbg2 = [SKSpriteNode spriteNodeWithTexture:bg0 size:textureSize];
        _mainbg2.anchorPoint = CGPointZero;
        _mainbg2.position = CGPointMake(_mainbg.size.width-1, 0);
        _mainbg2.name = @"layer05";
        
        //mountain00
        _mt00 = [SKSpriteNode spriteNodeWithTexture:mt0 size:textureSize];
        _mt00.anchorPoint = CGPointZero;
        _mt00.position = CGPointMake(0, self.frame.origin.y);
        _mt00.name = @"layer03";
        
        //mountain01
        _mt01 = [SKSpriteNode spriteNodeWithTexture:mt0 size:textureSize];
        _mt01.anchorPoint = CGPointZero;
        _mt01.position = CGPointMake(_mt00.size.width-1, 0);
        _mt01.name = @"layer03";
        
        //ground00
        _bg00 = [SKSpriteNode spriteNodeWithTexture:gd0];
        _bg00.size = textureSize;
        _bg00.physicsBody = [SKPhysicsBody bodyWithTexture:gd0 size:_bg00.size];
        _bg00.physicsBody.dynamic = NO;
        _bg00.physicsBody.categoryBitMask = groundCategory;
        _bg00.physicsBody.collisionBitMask = groundCategory | playerCategory;
        _bg00.physicsBody.contactTestBitMask = groundCategory | playerCategory;
        _bg00.position = CGPointMake(_bg00.size.width*.5, textureSize.height*.5);
        _bg00.name = @"layer00";
        
        //ground01
        _bg01 = [SKSpriteNode spriteNodeWithTexture:gd0];
        _bg01.size = textureSize;
        _bg01.physicsBody = [SKPhysicsBody bodyWithTexture:gd0 size:_bg01.size];
        _bg01.physicsBody.dynamic = NO;
        _bg01.physicsBody.categoryBitMask = groundCategory;
        _bg01.physicsBody.collisionBitMask = groundCategory | playerCategory;
        _bg01.physicsBody.contactTestBitMask = groundCategory | playerCategory;
        _bg01.position = CGPointMake(_bg00.size.width*1.5, textureSize.height*.5);
        _bg01.name = @"layer01";
        
        
        [self addChild:_mainbg];
        [self addChild:_mainbg2];
        [self addChild:_mt00];
        [self addChild:_mt01];
        [self addChild:_bg00];
        [self addChild:_bg01];
    }
    
    return self;
}

- (void)update
{
    _mainbg.position = CGPointMake(_mainbg.position.x-0.5, _mainbg.position.y);
    _mainbg2.position = CGPointMake(_mainbg2.position.x-0.5, _mainbg2.position.y);
    
     _bg00.position = CGPointMake(_bg00.position.x-25, _bg00.position.y);
     _bg01.position = CGPointMake(_bg01.position.x-25, _bg01.position.y);
     
     _mt00.position = CGPointMake(_mt00.position.x-1, _mt00.position.y);
     _mt01.position = CGPointMake(_mt01.position.x-1, _mt01.position.y);
     
     //ground
     if(_bg00.position.x < -_bg00.size.width*.5){
         _bg00.position = CGPointMake(_bg01.position.x + _bg01.size.width, _bg00.position.y);
     }
     
     if(_bg01.position.x < -_bg01.size.width*.5){
         _bg01.position = CGPointMake(_bg00.position.x + _bg00.size.width, _bg01.position.y);
     }
  
    //mountain
     if(_mt00.position.x < -_mt00.size.width){
         _mt00.position = CGPointMake(_mt01.position.x + _mt01.size.width, _mt00.position.y);
     }
     
     if(_mt01.position.x < -_mt01.size.width){
         _mt01.position = CGPointMake(_mt00.position.x + _mt00.size.width, _mt01.position.y);
     }
    
    //background
    if(_mainbg.position.x < -_mainbg.size.width){
        _mainbg.position = CGPointMake(_mainbg.position.x + _mainbg.size.width, _mainbg.position.y);
    }
    
    if(_mainbg2.position.x < -_mainbg2.size.width){
        _mainbg2.position = CGPointMake(_mainbg.position.x + _mainbg.size.width, _mainbg2.position.y);
    }
    
}


@end
