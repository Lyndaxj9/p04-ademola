//
//  MainBG.m
//  p04-ademola
//
//  Created by Lynda on 3/13/17.
//  Copyright © 2017 Lynda. All rights reserved.
//

#import "MainBG.h"

@implementation MainBG

- (SKSpriteNode *) init
{
    SKTextureAtlas *bgs = [SKTextureAtlas atlasNamed:@"background"];
    SKTexture *bg0 = [bgs textureNamed:@"layer_05"];
    
    _mainbg = [SKSpriteNode spriteNodeWithTexture:bg0];
    _mainbg.anchorPoint = CGPointZero;
    _mainbg.position = CGPointMake(0, 0);
    _mainbg.name = @"layer05";
    
    return _mainbg;
}

- (SKSpriteNode *) initWithSize:(CGSize)textureSize
{
    SKTextureAtlas *bgs = [SKTextureAtlas atlasNamed:@"background"];
    SKTexture *bg0 = [bgs textureNamed:@"layer_05"];
    
    _mainbg = [SKSpriteNode spriteNodeWithTexture:bg0 size:textureSize];
    _mainbg.anchorPoint = CGPointZero;
    _mainbg.position = CGPointMake(0, 0);
    _mainbg.name = @"layer05";
    
    return _mainbg;
}

- (void)update
{
    _mainbg.position = CGPointMake(_mainbg.position.x-0.5, _mainbg.position.y);
    
    if(_mainbg.position.x < -_mainbg.size.width){
        _mainbg.position = CGPointMake(_mainbg.position.x + _mainbg.size.width, _mainbg.position.y);
    }

}
@end
