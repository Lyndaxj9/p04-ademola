//
//  MainBG.h
//  p04-ademola
//
//  Created by Lynda on 3/13/17.
//  Copyright © 2017 Lynda. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface MainBG : SKSpriteNode
@property (nonatomic) SKSpriteNode *mainbg;

- (SKSpriteNode *) initWithSize:(CGSize)textureSize;
- (void)update;
@end
