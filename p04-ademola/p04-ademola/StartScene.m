//
//  StartScene.m
//  p04-ademola
//
//  Created by Lynda on 3/4/17.
//  Copyright © 2017 Lynda. All rights reserved.
//

#import "StartScene.h"
#import "MainBG.h"

@interface StartScene ()
@property BOOL contentCreated;
@end

@implementation StartScene

static const uint32_t playerCategory = 0x1 << 1;

- (void)didMoveToView:(SKView *)view
{
    if (!self.contentCreated)
    {
        [self createSceneContents];
        self.contentCreated = YES;
    }
}

- (void)createSceneContents
{
    self.backgroundColor = [SKColor orangeColor];
    self.scaleMode = SKSceneScaleModeAspectFit;
    
    //character sprite
    SKTextureAtlas *mgs = [SKTextureAtlas atlasNamed:@"mageavatar"];
    SKTexture *mg0 = [mgs textureNamed:@"wizard_1_run_001"];
    SKTexture *mg1 = [mgs textureNamed:@"wizard_1_run_002"];
    SKTexture *mg2 = [mgs textureNamed:@"wizard_1_run_003"];
    SKTexture *mg3 = [mgs textureNamed:@"wizard_1_run_004"];
    SKTexture *mg4 = [mgs textureNamed:@"wizard_1_run_005"];
    SKTexture *mg5 = [mgs textureNamed:@"wizard_1_run_006"];
    SKTexture *mg6 = [mgs textureNamed:@"wizard_1_run_007"];
    SKTexture *mg7 = [mgs textureNamed:@"wizard_1_run_008"];
    SKTexture *mj1 = [mgs textureNamed:@"wizard_1_jump_001"];
    SKTexture *mj2 = [mgs textureNamed:@"wizard_1_jump_002"];
    SKTexture *mj3 = [mgs textureNamed:@"wizard_1_jump_003"];
    SKTexture *mj4 = [mgs textureNamed:@"wizard_1_jump_004"];
    SKTexture *mgi = [mgs textureNamed:@"wizard_1"];

    NSArray *mgt = @[mg0, mg1, mg2, mg3, mg4, mg5, mg6, mg7];
    SKAction *mr = [SKAction animateWithTextures:mgt timePerFrame:0.4];
    NSArray *mjt = @[mj1, mj2, mj3, mj4];
    mj = [SKAction animateWithTextures:mjt timePerFrame:0.1];
    
    //background image in SKNode (bundle)
    aBKG = [[BKG alloc]initWithSize:self.frame.size];
    [self addChild:aBKG];
    NSLog(@"y size: %f", self.frame.size.height);
    
    //platforms in SKNode
    aPlatforms = [[Platforms alloc]init];
    [self addChild:aPlatforms];
    
    //avatar
    ava = [SKSpriteNode spriteNodeWithTexture:mgi];
    ava.physicsBody = [SKPhysicsBody bodyWithTexture:mgi size:mgi.size];
    ava.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(28, 32)];
    ava.physicsBody.usesPreciseCollisionDetection = YES;
    ava.physicsBody.allowsRotation = NO;
    ava.physicsBody.friction = 0.0;
    //ava.physicsBody.dynamic = NO;
    ava.physicsBody.categoryBitMask = playerCategory;
    //ava.anchorPoint = CGPointZero;
    ava.position = CGPointMake(100, 200);
    //ava.zPosition = 10;
    [self addChild:ava];
    [ava runAction:[SKAction repeatActionForever:mr]];
    

}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    for(UITouch *touch in touches)
    {
        NSLog(@"touch so jump");
        //SKAction *jump = [SKAction moveByX:0 y:52 duration:0.7];
        //[ava runAction:jump];
        [self jumpSprite:ava];
    }
}

- (void)jumpSprite:(SKSpriteNode *)obj
{
    [ava runAction:mj];
    CGFloat impulseX = 0.0f;
    CGFloat impulseY = 20.0f;
    [obj.physicsBody applyImpulse:CGVectorMake(impulseX, impulseY) atPoint:obj.position];
    
}



-(void)update:(CFTimeInterval)currentTime {
    // Called before each frame is rendered
    
    [aBKG update];
    [aPlatforms update];
    if(ava.position.x < 0+ava.size.width/2)
    {
        ava.position = CGPointMake(ava.size.width, ava.position.y);
    }
    
}

@end
