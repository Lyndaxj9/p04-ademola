//
//  BKG.h
//  p04-ademola
//
//  Created by Lynda on 3/14/17.
//  Copyright © 2017 Lynda. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface BKG : SKNode
@property (nonatomic) SKSpriteNode *mainbg, *mainbg2, *mt00, *mt01, *bg00, *bg01;

- (BKG *) initWithSize:(CGSize)textureSize;
- (void)update;
@end
